//
//  GenericView.swift
//  Weather App
//
//  Created by Afonso Rosa on 12/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

class GenericView: UIView {

    @IBOutlet var view: UIView!
    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setupNib() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        self.view.frame = bounds
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(self.view)
    }
    
    private func setup(){
        self.setupNib()
    }

}
