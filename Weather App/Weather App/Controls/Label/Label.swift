//
//  Label.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit
@IBDesignable
class Label: UILabel {

    @IBInspectable var theme: String = ""
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        LabelStyle.getStyleFromTheme(self.theme)
        self.font = UIFont(name: LabelStyle.fontName, size: LabelStyle.fontSize)
        self.textColor = LabelStyle.textColor
        self.alpha = LabelStyle.alpha
    }
    

}
