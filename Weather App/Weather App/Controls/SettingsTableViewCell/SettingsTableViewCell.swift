//
//  SettingsTableViewCell.swift
//  Weather App
//
//  Created by Afonso Rosa on 13/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

protocol SettingsTableViewCellDelegate : class {
    func textFieldDidEndEditing(_ text: String)
}

class SettingsTableViewCell: UITableViewCell {
    
    static let identifier = "SettingsTableViewCell"

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var data : SettingsCellData?
    weak var delegate: SettingsTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.textField.delegate = self
        
        LabelStyle.grayLightTheme1()
        self.textField.font = UIFont(name: LabelStyle.fontName, size: LabelStyle.fontSize)
        self.textField.textColor = LabelStyle.textColor
        self.textField.alpha = LabelStyle.alpha
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadWithData(_ data: SettingsCellData) {
        self.descriptionLabel.text = data.cellDescription
        self.textField.text = data.value
        self.data = data
        
        self.textField.isUserInteractionEnabled = data.canEdit()
        
        if data.type == .ForecastDaysCellType {
            self.textField.keyboardType = .numberPad
        }
    }
    
}

extension SettingsTableViewCell : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let data = self.data {
            if data.type == .ForecastDaysCellType {
                if (range.location > 0) {
                    return false
                }
                if let value = Int(string) {
                    return data.shouldEdit(value: value)
                }
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let data = self.data {
            if data.type == .ForecastDaysCellType {
                if let text = textField.text {
                    self.delegate?.textFieldDidEndEditing(text)
                }
            }
        }
    }
}
