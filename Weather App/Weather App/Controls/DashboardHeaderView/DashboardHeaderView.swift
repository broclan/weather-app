//
//  DashboardHeaderView.swift
//  Weather App
//
//  Created by Afonso Rosa on 12/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

protocol DashboardHeaderViewDelegate : class {
    func didTouchInfoIcon()
    func didTouchSettingsIcon()
}

class DashboardHeaderView: GenericView {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var mininumTemperatureLabel: UILabel!
    @IBOutlet weak var maximumTemperatureLabel: UILabel!
    
    weak var delegate: DashboardHeaderViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.view.backgroundColor = UIColor.mBlue1
    }
    
    func loadWithCity(_ city : String, weatherDescription : String, weather: String, currentTemperature: String, day: String, date: String, minTemperature: String, maxTemperature: String) {
        
        self.cityLabel.text = city
        self.weatherLabel.text = weatherDescription
        
        self.weatherImage.image = UIImage.init(named: weather)
        
        self.weatherImage.image = self.weatherImage.image!.withRenderingMode(.alwaysTemplate)
        self.weatherImage.tintColor = .white
        
        self.temperatureLabel.text = currentTemperature
        
        self.dayLabel.text = day
        self.dateLabel.text = date
        self.mininumTemperatureLabel.text = minTemperature
        self.maximumTemperatureLabel.text = maxTemperature
    }
    
    //MARK: Actions
    @IBAction func didTouchInfoIcon(_ sender: Any) {
        self.delegate?.didTouchInfoIcon()
    }
    
    @IBAction func didTouchSettingsIcon(_ sender: Any) {
        self.delegate?.didTouchSettingsIcon()
    }
    
    //MARK: Helpers
    
    func getCurrentHeight() -> CGFloat {
        return dayLabel.frame.maxY + 10.0
    }

}
