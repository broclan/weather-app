//
//  DailyWeatherTableViewCell.swift
//  Weather App
//
//  Created by Afonso Rosa on 12/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

class DailyWeatherTableViewCell: UITableViewCell {
    
    static let identifier = "DailyWeatherTableViewCell"
    
    //outlets
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var mininumTemperatureLabel: UILabel!
    @IBOutlet weak var maximumTemperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
