//
//  UIColor+Additions.swift
//  Weather App
//
//  Created by Afonso Rosa on 12/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

//extension for custom colors
extension UIColor {
    static let mBlue1 = UIColor.init(red: 33, green: 150, blue: 243)
    
    static let mGray1 = UIColor.init(red: 189, green: 189, blue: 189)
    static let mGray2 = UIColor.init(red: 235, green: 235, blue: 235)
    static let mGray3 = UIColor.init(red: 136, green: 136, blue: 136)
    
    static let mYellow1 = UIColor.init(red: 248, green: 205, blue: 103)
}

//extension for HEX
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        
        let nRed = red >= 0 && red <= 255 ? red : (red < 0 ? 0 : 255)
        let nGreen = green >= 0 && green <= 255 ? green : (green < 0 ? 0 : 255)
        let nBlue = blue >= 0 && blue <= 255 ? blue : (blue < 0 ? 0 : 255)
        
        self.init(red: CGFloat(nRed) / 255.0, green: CGFloat(nGreen) / 255.0, blue: CGFloat(nBlue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }
}
