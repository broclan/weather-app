//
//  Style.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation
import UIKit

struct LabelStyle {
    
    static var fontSize : CGFloat = 16
    static var fontName = "Lato-Regular"
    static var textColor = UIColor.white
    static var alpha : CGFloat = 1
    
    private static let themes = ["whiteRegularTheme1", "whiteRegularTheme2", "whiteRegularTheme3", "whiteLightTheme1", "whiteLightTheme2", "blackRegularTheme1", "blackRegularTheme2", "grayRegularTheme1", "grayLightTheme1"]
    
    //MARK: White Regular Color Schemes
    static func whiteRegularTheme1() {
        self.fontSize = 16
        self.textColor = .white
    }
    
    static func whiteRegularTheme2() {
        self.fontSize = 24
        self.textColor = .white
    }
    
    static func whiteRegularTheme3() {
        self.fontSize = 48
        self.textColor = .white
    }
    
    //MARK: White Light Color Schemes
    static func whiteLightTheme1() {
        self.fontSize = 16
        self.textColor = .white
        self.alpha = 0.8
    }
    
    static func whiteLightTheme2() {
        self.fontSize = 18
        self.textColor = .white
        self.alpha = 0.8
    }
    
    //MARK: Black Color Schemes
    static func blackRegularTheme1() {
        self.fontSize = 16
        self.textColor = .black
    }
    
    static func blackRegularTheme2() {
        self.fontSize = 18
        self.textColor = .black
    }
    
    //MARK: Grey Color Schemes
    static func grayRegularTheme1() {
        self.fontSize = 16
        self.textColor = UIColor.mGray1
    }
    
    //MARK: Grey Light Color Schemes
    static func grayLightTheme1() {
        self.fontSize = 16
        self.textColor = UIColor.mGray3
    }
    
    static func getStyleFromTheme(_ theme: String) {
        
        if let index = self.themes.index(of: theme) {
            switch index {
            case 0:
                whiteRegularTheme1()
                break;
            case 1:
                whiteRegularTheme2()
                break
            case 2:
                whiteRegularTheme3()
                break
            case 3:
                whiteLightTheme1()
                break
            case 4:
                whiteLightTheme2()
                break
            case 5:
                blackRegularTheme1()
                break
            case 6:
                blackRegularTheme2()
                break
            case 7:
                grayRegularTheme1()
                break
            case 8:
                grayLightTheme1()
                break
                
            default:
                whiteRegularTheme1()
            }
        }
    }
}
