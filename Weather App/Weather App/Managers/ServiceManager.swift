//
//  ServiceManager.swift
//  Weather App
//
//  Created by Afonso Rosa on 12/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation
import Alamofire

class ServiceManager {
    
    let apiKey = "80229664eaa27fee3618cc7603fad38e"
    
    static let sharedInstance = ServiceManager()
    private init() {} //This prevents others from using the default '()' initializer for this class.
    
    var cities : [City] = [] //temporary workaround while there isn't CoreData
    
    func getCityDataFromFileWithSuccess(success: @escaping ((_ data: [City]) -> Void)) {
        
        if self.cities.count > 0 {
            success(self.cities)
        }
        
        DispatchQueue.global(qos: .background).async {
            if let filePath = Bundle.main.path(forResource: "city.list", ofType: "json") {
                let data = try! Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
                
                var json: Any
                
                do {
                    json = try JSONSerialization.jsonObject(with: data)
                } catch {
                    return
                }
                
                guard let dictionary = json as? [JSON] else {
                    return
                }
                
                guard let cities = [City].from(jsonArray: dictionary) else {
                    return
                }
                
                self.cities = cities
                
                success(cities)
            }
        }
    }
    
    func getForecastForCity(_ city: City?, numberOfDays: Int, temperatureUnits: String, success: @escaping ((_ data: [SingleForecast]) -> Void)) {
        
        var id : Float = 0
        
        if let c = city, let i = c.id {
            id = i
        } else {
            id = 524901 //mocked
        }
        
        let cityId = String(format: "%.0f", id)
        let url = "http://samples.openweathermap.org/data/2.5/forecast?id=\(cityId)&appid=\(self.apiKey)"
        
        self.getRequestWithUrl(url, success: { (data) in
            
            guard let forecast = Forecast(json: data) else {
                return
            }
            
            success(self.proccessForecast(forecast, forTheNextDays: numberOfDays))
        }, error: { (error) in
            success([])
        })
    }
    
    private func getRequestWithUrl(_ url : String, success: @escaping ((_ data: JSON) -> Void), error: @escaping ((_ errorMessage: String) -> Void)) {
        
        Alamofire.request(url).responseJSON { response in
            
            guard response.result.isSuccess else {
                if let errorMessage = response.result.error {
                    print("Error while fetching tags: \(errorMessage)")
                    error("Error while fetching tags: \(errorMessage)")
                }
                return
            }
            
            guard let responseJSON = response.result.value as? JSON else {
                print("Invalid tag information received from the service")
                error("Invalid tag information received from the service")
                return
            }
            
            success(responseJSON)
        }
    }
    
    //MARK: Helpers
    private func proccessForecast(_ forecast: Forecast, forTheNextDays days: Int) -> [SingleForecast] {
        
        var forecastDays : [SingleForecast] = []
        
        if let forecastList = forecast.list {
            for single in forecastList {
                if forecastDays.count == 0 {
                    forecastDays.append(single)
                } else if let dateString = single.dtTxt {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    if let date = dateFormatter.date(from: dateString) {
                        let hour = Calendar.current.component(.hour, from: date)
                        if hour == 0 { //we just need one singleforecast from each day
                            forecastDays.append(single)
                        }
                    }
                }
                
                if forecastDays.count == days {
                    break
                }
            }
        }
        
        return forecastDays
    }
}
