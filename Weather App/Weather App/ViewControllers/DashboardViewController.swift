//
//  DashboardViewController.swift
//  Weather App
//
//  Created by Afonso Rosa on 12/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    static let informationViewControllerSegueIdentifier = "InformationViewControllerSegueIdentifier"
    static let settingsViewControllerSegueIdentifier = "SettingsViewControllerSegueIdentifier"

    //outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //vc vars
    var weatherData : [SingleForecast] = []
    var currentTemperature = "Celsius"
    var currentCity = "Rotterdam"
    var currentCityObject : City?
    var comesFromLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingView.backgroundColor = UIColor.mGray1

        tableView.register(UINib(nibName: "DailyWeatherTableViewCell", bundle: nil), forCellReuseIdentifier: DailyWeatherTableViewCell.identifier)
        
        if let temperatureUnits = UserDefaults.standard.string(forKey: SettingsViewController.temperatureUnisString) {
            self.currentTemperature = temperatureUnits
        }
        
        
        tableView.tableFooterView = UIView()
        
        if let data = UserDefaults.standard.value(forKey:SettingsViewController.locationString) as? Data {
            if let city = try? PropertyListDecoder().decode(City.self, from: data) {
                if let name = city.name {
                    if self.currentCity != name {
                        self.currentCity = name
                        self.currentCityObject = city
                    }
                }
            }
        }
        
        if let temperatureUnits = UserDefaults.standard.string(forKey: SettingsViewController.temperatureUnisString) {
            if temperatureUnits != self.currentTemperature {
                self.currentTemperature = temperatureUnits
            }
        }
        
        var days = UserDefaults.standard.integer(forKey: SettingsViewController.forecastDaysString)
        
        if days == 0 {
            days = 5
        }
        
        self.getForecastForTheNextDays(days)
        
        self.comesFromLoad = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if self.comesFromLoad {
            self.comesFromLoad = false
            return
        }
        
        var reloadNeeded = false
        
        if let data = UserDefaults.standard.value(forKey:SettingsViewController.locationString) as? Data {
            if let city = try? PropertyListDecoder().decode(City.self, from: data) {
                if let name = city.name {
                    if self.currentCity != name {
                        self.currentCity = name
                        self.currentCityObject = city
                        reloadNeeded = true
                    }
                }
            }
        }
        
        if let temperatureUnits = UserDefaults.standard.string(forKey: SettingsViewController.temperatureUnisString) {
            if temperatureUnits != self.currentTemperature {
                self.currentTemperature = temperatureUnits
                reloadNeeded = true
            }
        }
        
        let days = UserDefaults.standard.integer(forKey: SettingsViewController.forecastDaysString)
        
        if days != self.weatherData.count {
            reloadNeeded = true
        }
        
        if reloadNeeded {
            self.getForecastForTheNextDays(days)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    //MARK: Services
    func getForecastForTheNextDays(_ days: Int) {
        self.startLoading()
        
        ServiceManager.sharedInstance.getForecastForCity(self.currentCityObject, numberOfDays: days, temperatureUnits: self.currentTemperature) {
            (data) in
            
            if data.count == 0 {
                 self.stopLoading()
                return
            }
            
            self.weatherData = data
            
            self.stopLoading()
            self.setupHeaderView()
            self.tableView.reloadData()
        }
    }
    
    //MARK: Helpers
    func setupHeaderView() {
        let headerView = DashboardHeaderView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 0.0))
        
        headerView.delegate = self
        
        
        var current : Float = 11.5
        var min : Float = 7.0
        var max : Float = 12.0
        
        var extendedDate = "Fri 10 Nov"
        var weatherIcon = "sun"
        var weatherDesc = "Clear"
        
        if self.weatherData.count > 0 {
            let forecast = self.weatherData[0]
            
            if let main = forecast.main, let c = main.temp, let mi = main.tempMin, let ma = main.tempMax {
                current = c
                min = mi
                max = ma
            }
            
            if let weather = forecast.weather, weather.count > 0, let w = weather[0].main {
                weatherDesc = self.getCorrespondentDescription(weather: w)
                weatherIcon = self.getCorrespondentIcon(weather: w)
            }
            
            
            if let dateString = forecast.dtTxt {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                if let date = dateFormatter.date(from: dateString) {
                    dateFormatter.dateFormat = "EEE dd LLL"
                    extendedDate = dateFormatter.string(from: date)
                }
            }
        }
        
        let currentTemperatureString = self.convertTemperature(degrees: current)
        let minTemperatureString = self.convertTemperature(degrees: min)
        let maxTemperatureString = self.convertTemperature(degrees: max)
        
        headerView.loadWithCity(self.currentCity, weatherDescription: weatherDesc, weather: weatherIcon, currentTemperature: "\(currentTemperatureString)º", day: NSLocalizedString("dashboard.today", comment: ""), date: extendedDate, minTemperature: minTemperatureString, maxTemperature: maxTemperatureString)
        
        headerView.frame = CGRect(x: 0, y: 0, width: headerView.frame.width, height: headerView.getCurrentHeight())
        
        self.tableView.tableHeaderView = headerView
    }
    
    func convertTemperature(degrees: Float) -> String {
        var convertedDegress = degrees
        
        if self.currentTemperature == "Celsius" {
            convertedDegress = convertKelvinToCelsius(degrees: convertedDegress)
        } else {
            convertedDegress = convertKelvinToFahrenheit(degrees: convertedDegress)
        }
        
        return String(format: "%.0f", convertedDegress)
    }
    
    func convertKelvinToCelsius(degrees: Float) -> Float {
        return degrees - 273.15
    }
    
    func convertKelvinToFahrenheit(degrees: Float) -> Float {
        return degrees * 9/5 - 459.67
    }
    
    func startLoading() {
        self.loadingView.isHidden = false
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.loadingView.isHidden = true
    }
    
    func getDayOfTheWeek(_ day: Int) -> String {
        switch day {
            case 1:
                return NSLocalizedString("dashboard.monday", comment: "")
            case 2:
                return NSLocalizedString("dashboard.tuesday", comment: "")
            case 3:
                return NSLocalizedString("dashboard.wednesday", comment: "")
            case 4:
                return NSLocalizedString("dashboard.thrusday", comment: "")
            case 5:
                return NSLocalizedString("dashboard.friday", comment: "")
            case 6:
                return NSLocalizedString("dashboard.saturday", comment: "")
            case 7:
                return NSLocalizedString("dashboard.sunday", comment: "")
            default:
                return ""
        }
    }
    
    func getCorrespondentIcon(weather: String) -> String {
        if weather == "Clear" {
            return "sun"
        }
        if weather == "Snow" || weather == "Rain" {
            return "rain"
        }
        if weather == "Clouds" {
            return "clouds"
        }
        if weather == "Extreme" {
            return "storm"
        }
        
        return "sun"
    }
    
    func getCorrespondentDescription(weather: String) -> String {
        if weather == "Clear" {
            return NSLocalizedString("dashboard.clear", comment: "")
        }
        if weather == "Snow" {
            return NSLocalizedString("dashboard.snow", comment: "")
        }
        if weather == "Rain" {
            return NSLocalizedString("dashboard.rain", comment: "")
        }
        if weather == "Clouds" {
            return NSLocalizedString("dashboard.clouds", comment: "")
        }
        if weather == "Extreme" {
            return NSLocalizedString("dashboard.extreme", comment: "")
        }
        
        return NSLocalizedString("dashboard.clear", comment: "")
    }

}

extension DashboardViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: DailyWeatherTableViewCell.identifier, for: indexPath) as? DailyWeatherTableViewCell {
            
            let forecast = weatherData[indexPath.row]
            
            var day = 0
            
            if let dateString = forecast.dtTxt {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                if let date = dateFormatter.date(from: dateString) {
                    day = Calendar.current.component(.weekday, from: date)
                }
            }
            cell.dayLabel?.text = indexPath.row == 0 ? NSLocalizedString("dashboard.tomorrow", comment: "") : self.getDayOfTheWeek(day)
            
            var weatherIcon = "sun"
            
            if let weather = forecast.weather, weather.count > 0, let w = weather[0].main {
                weatherIcon = self.getCorrespondentIcon(weather: w)
            }
            
            cell.weatherIcon?.image = UIImage.init(named: weatherIcon)
            cell.weatherIcon.isHidden = indexPath.row == 0
            
            if let main = forecast.main, let min = main.tempMin, let max = main.tempMax {
                
                cell.mininumTemperatureLabel?.text = self.convertTemperature(degrees: min)
                cell.maximumTemperatureLabel?.text = self.convertTemperature(degrees: max)
            }
            
            return cell
        } else {
            return UITableViewCell()
        }
        
    }
}

extension DashboardViewController : DashboardHeaderViewDelegate {
    func didTouchInfoIcon() {
        performSegue(withIdentifier: DashboardViewController.informationViewControllerSegueIdentifier, sender: self)
    }
    
    func didTouchSettingsIcon() {
        performSegue(withIdentifier: DashboardViewController.settingsViewControllerSegueIdentifier, sender: self)
    }
}
