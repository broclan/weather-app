//
//  SettingsViewController.swift
//  Weather App
//
//  Created by Afonso Rosa on 13/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    //MARK: constants
    let rowHeight : CGFloat = 50.0
    static let forecastDaysString = "ForecastDays"
    static let temperatureUnisString = "TemperatureUnis"
    static let locationString = "Location"

    //MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: vars
    var footerView: UIView?
    var cellData : [SettingsCellData] = []
    let pickerData = ["Fahrenheit", "Celsius"]
    var currentCity = "Rotterdam"
    var cities : [City] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("settings.title", comment: "")
        
        self.loadingView.backgroundColor = UIColor.mGray1
        
        tableView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: SettingsTableViewCell.identifier)
        tableView.backgroundColor = UIColor.mGray2
        
        self.setupFooterView()
        
        if let footer = self.footerView {
            self.tableView.tableFooterView = footer
        }
        
        self.pickerView.alpha = 0
        self.pickerView.backgroundColor = .white
        
        var index = 1
        
        if let temperatureUnits = UserDefaults.standard.string(forKey: SettingsViewController.temperatureUnisString) {
            if let i = self.pickerData.index(of: temperatureUnits) {
                index = i
            }
        }
        
        self.pickerView.selectRow(index, inComponent: 0, animated: false)
        
        self.setupCellData()
        
        self.getCityData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: Actions
    @IBAction func backButtonTouched(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Services
    func getCityData() {
        self.startLoading()
        ServiceManager.sharedInstance.getCityDataFromFileWithSuccess {
            (cities) -> Void in
            
            self.cities = cities
            self.stopLoading()
        }
    }
    
    //MARK: Helpers
    func startLoading() {
        self.loadingView.isHidden = false
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        self.loadingView.isHidden = true
    }
    
    func setupFooterView() {
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: rowHeight))
        
        footer.backgroundColor = UIColor.mBlue1
        
        let label = UILabel.init(frame: CGRect(x: 10, y: 10, width: self.tableView.frame.width - 20, height: 30))
        
        label.text = "v1.0.0"
        label.textColor = .white
        label.textAlignment = .center
        label.autoresizingMask = [.flexibleWidth, .flexibleLeftMargin, .flexibleRightMargin]
        
        footer.addSubview(label)
        
        self.footerView = footer
    }
    
    func setupCellData() {
        
        if let data = UserDefaults.standard.value(forKey:SettingsViewController.locationString) as? Data {
            if let city = try? PropertyListDecoder().decode(City.self, from: data) {
                if let name = city.name {
                    self.currentCity = name
                }
            }
        }
        
        let locationCell = SettingsCellData(description: NSLocalizedString("settings.location", comment: ""), value: self.currentCity, type: .LocationCellType)
        
        var units = "Celsius"
        
        if let temperatureUnits = UserDefaults.standard.string(forKey: SettingsViewController.temperatureUnisString) {
            units = temperatureUnits
        }
        
        let temperatureCell = SettingsCellData(description: NSLocalizedString("settings.temperatureUnits", comment: ""), value: units, type: .TemperatureCellType)
        
        let days = UserDefaults.standard.integer(forKey: SettingsViewController.forecastDaysString)
        
        let forecastDaysCell = SettingsCellData(description: NSLocalizedString("settings.forecastDays", comment: ""), value: String(days), type: .ForecastDaysCellType)
        
        self.cellData = [locationCell, temperatureCell, forecastDaysCell]
        
        self.tableView.reloadData()
    }
    
    func showLocationAlert() {
        let alert = UIAlertController(title: NSLocalizedString("settings.location", comment: ""), message: NSLocalizedString("settings.enterCityMessage", comment: ""), preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = NSLocalizedString("settings.city", comment: "")
        }
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("general.cancel", comment: ""), style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            if let locationAlert = alert {
                if let textFields = locationAlert.textFields, textFields.count > 0 {
                    if let text = textFields[0].text {
                        self.processLocationAlertText(text)
                    }
                }
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func processLocationAlertText(_ text : String) {
        if text == self.currentCity {
            return
        }
        self.startLoading()
        
        var exists = false
        
        var selectedCity : City?
        
        for city in self.cities {
            if let name = city.name {
                if name == text {
                    selectedCity = city
                    exists = true
                    break;
                }
            }
        }
        
        self.stopLoading()
        if exists { //if exists is true, then selectedCity has something
            UserDefaults.standard.set(try? PropertyListEncoder().encode(selectedCity), forKey: SettingsViewController.locationString)
            
            self.setupCellData()
        } else {
            let alert = UIAlertController(title: NSLocalizedString("general.error", comment: ""), message: NSLocalizedString("settings.cityDoesntExist", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func hidePickerView() {
        if self.pickerView.alpha == 0 {
            return
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {
            self.pickerView.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func showPickerView() {
        if self.pickerView.alpha == 1 {
            return
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3) {
            self.pickerView.alpha = 1
            self.view.layoutIfNeeded()
        }
    }

}

extension SettingsViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellData.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == self.cellData.count {
            return self.tableView.frame.size.height > CGFloat(self.cellData.count + 1) * rowHeight ? self.tableView.frame.size.height - CGFloat(self.cellData.count + 1) * rowHeight : 0
        }
        
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row < self.cellData.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTableViewCell.identifier, for: indexPath) as? SettingsTableViewCell {
                
                cell.delegate = self
                
                cell.loadWithData(self.cellData[indexPath.row])
                
                if indexPath.row == self.cellData.count - 1 { //hide the separator of the last cell with data
                    cell.separatorInset = UIEdgeInsetsMake(0, cell.bounds.size.width, 0, 0)
                }
                
                return cell
            }
        }
        
        let cell = UITableViewCell()
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        
        if indexPath.row < self.cellData.count {
            let data = self.cellData[indexPath.row]
            if data.type == .TemperatureCellType {
                self.showPickerView()
                return
            } else if data.type == .LocationCellType {
                self.showLocationAlert()
            }
        }
        
        self.hidePickerView()
    }
}

extension SettingsViewController : SettingsTableViewCellDelegate {
    func textFieldDidEndEditing(_ text: String) {
        let days = UserDefaults.standard.integer(forKey: SettingsViewController.forecastDaysString)
        
        if days != Int(text) {
            UserDefaults.standard.set(Int(text), forKey: SettingsViewController.forecastDaysString)
        }
    }
}

extension SettingsViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let selectedTemperatureUnits = self.pickerData[row]
        
        if let temperatureUnits = UserDefaults.standard.string(forKey: SettingsViewController.temperatureUnisString) {
            if temperatureUnits == selectedTemperatureUnits {
                return
            }
        }
        UserDefaults.standard.set(selectedTemperatureUnits, forKey: SettingsViewController.temperatureUnisString)
        
        self.setupCellData()
    }
}
