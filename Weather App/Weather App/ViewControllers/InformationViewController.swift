//
//  InformationViewController.swift
//  Weather App
//
//  Created by Afonso Rosa on 13/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var distanceToLastElement: NSLayoutConstraint!
    @IBOutlet weak var versionView: UIView!
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("information.title", comment: "")
        
        self.view.backgroundColor = UIColor.mGray2
        
        self.informationLabel.text = NSLocalizedString("information.description", comment: "")
        self.versionLabel.text = "v1.0.0"
        
        self.versionView.backgroundColor = UIColor.mBlue1
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.calculateDistanceToLastElement()
    }
    
    //MARK: Actions
    @IBAction func backButtonTouched(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Helpers
    func calculateDistanceToLastElement() {
        //if the contentSize and the frame are the same, no need to recalculate
        if self.scrollView.contentSize.height == self.scrollView.frame.height {
            return
        }
        
        self.view.setNeedsDisplay()
        self.distanceToLastElement.constant = 0
        self.view.layoutIfNeeded()
        
        //if the scroll view doesn't occupy the whole view, add space between the last two elements
        if self.scrollView.contentSize.height < self.scrollView.frame.height && self.scrollView.contentSize.height != 0 {
            let distance = self.scrollView.frame.height - self.scrollView.contentSize.height
            self.distanceToLastElement.constant = distance
            self.view.setNeedsLayout()
        } else if self.scrollView.contentSize.height > self.scrollView.frame.height && self.scrollView.contentSize.height != 0 {
            //if the scroll view is bigger than the view, add a margin between the last two elements
            self.distanceToLastElement.constant = 10.0
            self.view.setNeedsLayout()
        }
    }
}
