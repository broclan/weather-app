//
//  City.swift
//  Weather App
//
//  Created by Afonso Rosa on 14/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

struct City: JSONDecodable, Codable {
    
    let id: Float?
    let name: String?
    let country: String?
    let coord: Coordinate?
    
    init?(json: JSON) {
        self.id = "id" <~~ json
        self.name = "name" <~~ json
        self.country = "country" <~~ json
        self.coord = "coord" <~~ json
    }

}
