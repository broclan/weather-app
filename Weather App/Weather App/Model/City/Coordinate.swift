//
//  Coordinate.swift
//  Weather App
//
//  Created by Afonso Rosa on 14/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

struct Coordinate: JSONDecodable, Codable {
    
    let lon: Float?
    let lat: Float?
    
    init?(json: JSON) {
        self.lon = "lon" <~~ json
        self.lat = "lat" <~~ json
    }
}
