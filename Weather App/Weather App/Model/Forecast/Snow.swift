//
//  Snow.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct Snow : JSONDecodable {
    
    let threeHours: Float?
    
    init?(json: JSON) {
        self.threeHours = "3h" <~~ json
    }
}
