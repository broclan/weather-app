//
//  SingleForecast.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct SingleForecast: JSONDecodable {

    let dt: Float?
    let main: MainForecast?
    let weather: [Weather]?
    let clouds: Clouds?
    let wind: Wind?
    let snow: Snow?
    let sys: Sistem?
    let dtTxt: String?
    
    init?(json: JSON) {
        self.dt = "dt" <~~ json
        self.main = "main" <~~ json
        self.clouds = "clouds" <~~ json
        self.wind = "wind" <~~ json
        self.snow = "snow" <~~ json
        self.sys = "sys" <~~ json
        self.dtTxt = "dt_txt" <~~ json
        
        guard let weatherJSON: [JSON] = "weather" <~~ json, let weather = [Weather].from(jsonArray: weatherJSON) else {
            self.weather = []
            return
        }
        self.weather = weather
    }
}
