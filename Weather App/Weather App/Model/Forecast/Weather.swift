//
//  Weather.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct Weather : JSONDecodable {
    
    let id: Float?
    let main: String?
    let weatherDescription : String?
    let icon: String?
    
    init?(json: JSON) {
        self.id = "id" <~~ json
        self.main = "main" <~~ json
        self.weatherDescription = "description" <~~ json
        self.icon = "icon" <~~ json
    }
}
