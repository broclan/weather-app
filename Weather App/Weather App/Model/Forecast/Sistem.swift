//
//  Sistem.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct Sistem : JSONDecodable {
    let pod: String?
    
    init?(json: JSON) {
        self.pod = "pod" <~~ json
    }
}
