//
//  Forecast.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct Forecast: JSONDecodable {
    
    let cod: String?
    let message: Float?
    let cnt: Float?
    var list: [SingleForecast]?
    let city: City?
    
    init?(json: JSON) {
        self.cod = "cod" <~~ json
        self.message = "message" <~~ json
        self.cnt = "cnt" <~~ json
        self.city = "city" <~~ json
        
        guard let listJSON: [JSON] = "list" <~~ json, let list = [SingleForecast].from(jsonArray: listJSON) else {
            self.list = []
            return
        }
        self.list = list
    }
    
}
