//
//  MainForecast.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct MainForecast: JSONDecodable {
    
    let temp: Float?
    let tempMin: Float?
    let tempMax: Float?
    let pressure: Float?
    let seaLevel: Float?
    let grndLevel: Float?
    let humidity: Float?
    let tempKf: Float?

    init?(json: JSON) {
        self.temp = "temp" <~~ json
        self.tempMin = "temp_min" <~~ json
        self.tempMax = "temp_max" <~~ json
        self.pressure = "pressure" <~~ json
        self.seaLevel = "sea_level" <~~ json
        self.grndLevel = "grnd_level" <~~ json
        self.humidity = "humidity" <~~ json
        self.tempKf = "temp_kf" <~~ json
    }
}
