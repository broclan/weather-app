//
//  Wind.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct Wind : JSONDecodable {
    
    let speed: Float?
    let deg: Float?
    
    init?(json: JSON) {
        self.speed = "speed" <~~ json
        self.deg = "deg" <~~ json
    }
}
