//
//  Clouds.swift
//  Weather App
//
//  Created by Afonso Rosa on 15/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import Foundation

struct Clouds : JSONDecodable {
    
    let all: Float?
    
    init?(json: JSON) {
        self.all = "all" <~~ json
    }
}
