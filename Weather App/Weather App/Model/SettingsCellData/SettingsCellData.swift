//
//  SettingsCellData.swift
//  Weather App
//
//  Created by Afonso Rosa on 13/12/17.
//  Copyright © 2017 Afonso Rosa. All rights reserved.
//

import UIKit

enum SettingsCellType {
    case LocationCellType
    case TemperatureCellType
    case ForecastDaysCellType
    case None
}

class SettingsCellData: NSObject {
    
    var cellDescription : String = ""
    var value : String = ""
    var type: SettingsCellType = .None
    
    convenience init(description: String, value: String, type: SettingsCellType) {
        self.init()
        
        self.cellDescription = description
        self.value = value
        self.type = type
    }
    
    func canEdit() -> Bool {
        if self.type == .ForecastDaysCellType {
            return true
        }
        
        return false
    }
    
    func shouldEdit(value : Any) -> Bool {
        if self.type == .ForecastDaysCellType {
            
            if let number = value as? Int {
                return number > 0 && number < 6
            }
        }
        
        return false
    }

}
